## Contribute to confd-data-container-generator

Thank you for your interest in contributing to confd-data-container-generator.

To contribute to the code, the workflow is as follows:
1. Fork the project into your personal space on GitLab.com
1. Configure the "CI / CD Settings" (https://gitlab.com/<YOUR_ID>/confd-data-container-generator/settings/ci_cd) to allow to run tests for your fork:
  1. Add "secret variable" `DOCKER_HUB_PASSWORD` with your DockerHub ID
  1. Add "secret variable" `DOCKER_HUB_USERNAME` with your DockerHub password
1. Create a feature branch, branch away from `master`
1. Write code and tests
1. Push the commit(s) to your fork
1. Submit a merge request (MR) to the `master` branch
