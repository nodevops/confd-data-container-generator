#!/usr/bin/env bash

print_usage() {
    cat <<- EOF

    usage: ${PROG_NAME} parameters options

    Generate a file for export environment variables from dictionaries.
    Merge files from data into one single folder

    PARAMETERS:
       -r --repository         url for the cloneable repository
       -v --app-version        application version on the repository
       -e --environment        name of the environment

    OPTIONS:
       -h --help               print this help
       -w --working-directory  specify the working directory
                                (default value: ./tmp should be create before)
       -n --config-file-name   name of the output file generated
                                (default value: env.sh)
       -o --out-directory      folder where the generated file will be
                                (default value: ./config should be create before)

    Examples:
       ${PROG_NAME} -e dev-local -v v0.1.0 --repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git
EOF
}

print_state() {
    cat <<- EOF

    ##################################################################
    ${PROG_NAME}: $1
    $2 $3
    ##################################################################
EOF
}

is_here() {
    print_state "test if directory is created" "$FUNCNAME" "$@"
    local value=$1
    local name=$2
    if ! [ -d "$value" ]; then
        echo -e "$name: $value doesn't exist"
        exit 1
    fi
}

is_writeable() {
    print_state "test if directory is modifiable" "$FUNCNAME" "$@"
    local value=$1
    local name=$2
    if ! [ -w "$value" ]; then
        echo -e "$name: $value is not writeable"
        exit 1
    fi
}

check_architecture() {
    print_state "checking directories and files structure..." "$FUNCNAME" "$@"
    # Checking if all needed folder exist
    is_here "${OUT_DIR}" "${!OUT_DIR@}"
    is_here "${OUT_DIR_DATA}" "${!OUT_DIR_DATA@}"
    is_here "${OUT_DIR_DICT}" "${!OUT_DIR_DICT@}"
    is_here "${WORK_DIR}" "${!WORK_DIR@}"

    # Checking if folder are writeable
    is_writeable "${OUT_DIR_DATA}" "${!OUT_DIR_DATA@}"
    is_writeable "${OUT_DIR_DICT}" "${!OUT_DIR_DICT@}"
    is_writeable "${WORK_DIR}" "${!WORK_DIR@}"

    echo -e "Folder architecture conforms!"
}

import_repo() {
    print_state "cloning the git repo..." "$FUNCNAME" "$@"
    cd "${WORK_DIR}" \
        && git clone -b "${TAG}" "${REPO}" repo \
        || exit
    echo -e "Repository imported!"
}

generate_file_env() {
    print_state "generating the configuration..." "$FUNCNAME" "$@"
    # TODO: implement new output type (--env option)
    local arguments=""
    arguments="${arguments} --type env"
    arguments="${arguments} --env ${ENV}"
    arguments="${arguments} --file ${OUT_DIR_DICT}/${OUTPUT_FILE_CONFIG_NAME}"
    arguments="${arguments} --root-path ${WORK_DIR}/repo/dictionaries"
    if [ -e /usr/local/bin/generate-file-env.py ]; then
        local gfec=(generate-file-env.py  "${arguments}")
    else
        local gfec=(../generate-file-env.py  "${arguments}")
    fi
    eval "${gfec[@]}" \
        || exit
    echo -e "File ${OUTPUT_FILE_CONFIG_NAME} generated!"
}

merge_data() {
    print_state "merging the data directories..." "$FUNCNAME" "$@"
    cd "${WORK_DIR}/repo/data" \
        && cp -v common/* "${OUT_DIR_DATA}/" \
        && cp -v "${ENV}"/* "${OUT_DIR_DATA}/" \
        || exit
    echo -e "\tData merged!"
}

check_initialized() {
    print_state "test if this mandatory option is initialised" "$FUNCNAME" "$@"
    local name=$1
    local value=$2
    if ! [ -n "$value" ]; then
        echo -e "$name: $value is not initialized!"
        print_usage
        exit 1
    fi
}

args_manager() {
    local options
    local arguments=( $@ )
    # read the options
    local short_opts="hr:v:e:w:n:o:"
    local long_opts="help,repository:,app-version:,environment:,working-directory:,config-file-name:,out-directory:"
    if ! options=$(getopt --options ${short_opts} --longoptions ${long_opts} -- "${arguments[@]}"); then
        print_usage
        exit 1
    fi
    eval set -- "${options}"
    options=( $@ )

    # extract options and their arguments into variables.
    while true; do
        case "${options[0]}" in
            -r | --repository)
                REPO="${options[1]}"
                options=("${options[@]:2}") # equivalent at shift 2 for an array
                ;;
            -v | --app-version)
                APP_VERSION="${options[1]}"
                options=("${options[@]:2}")
                ;;
            -e | --environment)
                ENV="${options[1]}"
                options=("${options[@]:2}")
                ;;
            -w | --working-directory)
                WORK_DIR="${options[1]}"
                options=("${options[@]:2}")
                ;;
            -n | --config-file-name)
                OUTPUT_FILE_CONFIG_NAME="${options[1]}"
                options=("${options[@]:2}")
                ;;
            -o | --out-directory)
                OUT_DIR="${options[1]}"
                options=("${options[@]:2}")
                ;;
            -h | --help)
                print_usage
                exit 0
                ;;
            --)
                options=("${options[@]:1}")
                check_initialized "${!REPO@}" "${REPO}"
                check_initialized "${!APP_VERSION@}" "${APP_VERSION}"
                check_initialized "${!ENV@}" "${ENV}"
                TAG=${APP_VERSION}_${ENV}
                OUT_DIR_DATA=${OUT_DIR}/data
                OUT_DIR_DICT=${OUT_DIR}/dictionaries
                break
                ;;
            *)
                echo "${options[0]}"
                print_usage
                exit 1
                ;;
        esac
    done

    if test ${#options[@]} -ne 0 && test ${#options[0]} -ne 2; then
        echo "too many arguments: " "${options[*]}"
        print_usage
        exit 1
    fi
}
