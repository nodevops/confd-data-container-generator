#!/bin/bash

readonly PROG_NAME=$(basename "$0")
readonly ARGS=( "$@" )

set -x

if [ -e /usr/local/bin/libs/utils.sh ]; then
    source /usr/local/bin/libs/utils.sh
else
    source libs/utils.sh
fi

function main() {
    args_manager "${ARGS[@]}"

    print_state "runtime configuration"
    cat <<- EOF
    ENV=${ENV}
    APP_VERSION=${APP_VERSION}
    TAG=${TAG}
    REPO=${REPO}
    WORK_DIR=${WORK_DIR}
    OUT_DIR=${OUT_DIR}
    OUT_DIR_DATA=${OUT_DIR_DATA}
    OUT_DIR_DICT=${OUT_DIR_DICT}
    OUTPUT_FILE_CONFIG_NAME=${OUTPUT_FILE_CONFIG_NAME}
EOF

    check_architecture "" \
        || exit

    import_repo "" \
        || exit

    generate_file_env "" \
        || exit

    merge_data "" \
        || exit
}

###########################################################################################
# main
###########################################################################################

REPO=
ENV=
APP_VERSION=
TAG=${APP_VERSION}_${ENV}

# default global variables
WORK_DIR=$(pwd)/tmp
OUT_DIR=$(pwd)/config
OUT_DIR_DATA=${OUT_DIR}/data
OUT_DIR_DICT=${OUT_DIR}/dictionaries
OUTPUT_FILE_CONFIG_NAME=env.sh

main
