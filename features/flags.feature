Feature: Flags

  Scenario Outline: Invalid command line
    Given I initialized options
    Given I added flag "<invalid_flag>"
    When I launch generate-data-image.sh
    Then I should receive program exit with status "1"
    And I should receive an error message "<error_message>"
    And I should receive the help message

    Examples:
      | invalid_flag   | error_message      |
      | --unknown-flag | long option        |
      | -k             | option unknown     |
      | arg1           | too many arguments |
      | arg1 agr2 arg3 | too many arguments |

  Scenario Outline: Asking help
    Given I initialized options
    Given I added flag "<help>"
    When I launch generate-data-image.sh
    Then I should receive program exit with status "0"
    And I should receive the help message

    Examples:
      | help   |
      | --help |
      | -h     |

  Scenario Outline: Succeeded launching mandatory
    Given I initialized options
    Given I added flag "<repo>"
    And I added flag "<version>"
    And I added flag "<name>"
    When I launch generate-data-image.sh
    Then I should receive program exit with status "0"
    Then I should receive trace message

    Examples:
      | repo                                                                             | version                | name                  |
      | -g https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git               | -v v0.1.0              | -e dev-local          |
      | --git-repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git | --env-version  v0.1.0  | --env-name dev-local  |

  Scenario Outline: Quiet option
    Given I initialized options
    Given I added flag "<quiet>"
    And I initialized mandatory options
    When I launch generate-data-image.sh
    Then I should receive program exit with status "0"
    Then I should receive only the sha of the new image

    Examples:
      | quiet   |
      | --quiet |
      | -q      |

  Scenario Outline: All flags in one command
    Given I initialized options
    Given I initialized mandatory options
    And I added flag "<working-directory>"
    And I added flag "<config-file-name>"
    And I added flag "<config-base-directory>"
    And I added flag "<image-name>"
    When I launch generate-data-image.sh
    Then I should receive program exit with status "0"
    Then I should receive trace message

    Examples:
      | working-directory            |config-file-name           | config-base-directory           | image-name                 |
      | -w /var/tmp                  | -f env.sh                 | -d /config                      | -i test-cucumber           |
      | --working-directory /var/tmp | --config-file-name env.sh | --config-base-directory /config | --image-name test-cucumber |

  Scenario Outline: Docker cache option activated
    Given I have already loaded the docker cache
    Given I initialized options
    Given I initialized mandatory options
    And I added flag "<no-cache>"
    When I launch generate-data-image.sh
    Then I should receive program exit with status "0"
    Then I should receive trace message
    And I should not see usage of docker cache

    Examples:
      | no-cache   |
      | -n         |
      | --no-cache |

  Scenario: Docker cache option unactivated
    Given I have already loaded the docker cache
    Given I initialized options
    Given I initialized mandatory options
    When I launch generate-data-image.sh
    Then I should receive program exit with status "0"
    Then I should receive trace message
    And I should see docker cache used

 Scenario Outline: Push option
    Given I initialized options
    Given I initialized mandatory options
    And I added flag "<push>"
    Given I was logged
    Given I generated image name
    And I checked if the name is single
    When I launch generate-data-image.sh
    Then I should receive program exit with status "0"
    Then I should receive trace message
    Then I should be able to pull this image
    And I have to delete it
    And I must be logged out

    Examples:
      | push            |
      | -p              |
      | --publish-image |
