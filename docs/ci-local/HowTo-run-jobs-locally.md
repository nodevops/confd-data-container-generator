# How to run jobs locally

## Purpose

A job is a part of Continuous Integration. 
During this job you will for example run unit test for a part of your application.
You could also test the compilation in another job.
Or to deploy your application via job using shell runner. 
 
Usually the CI is run when a push occurs on a branch.
To test your CI jobs you have to push first. 
This mandatory step is boring because you might push something wrong not inside the code itself but inside the test code to run the CI.

By running jobs locally, you can test your configuration of CI and the result too.
Like this you can run CI before push and avoid a wrong push.

Jobs can run with multiple runners but the easiest one is **Docker**.
You can test everything inside docker image without thing about your environment.

## Via command line

Template:
```bash
gitlab-ci-multi-runner exec [runner] [job-name]
```

Example:
```bash
gitlab-ci-multi-runner exec docker script-python
gitlab-ci-multi-runner exec docker script-bash
```
