# Prerequisites for run the application

For the current version of the application, we assume some prerequisites.
We plan to reduce these at the minimum during next sprint and next version.

Currently we need:
* docker installed (if not click [here](https://docs.docker.com/engine/installation/linux/))
* user part of docker group, be able to run docker without `sudo` (if not click [here](https://askubuntu.com/questions/477551/how-can-i-use-docker-without-sudo))
* already logged to *docker hub*, (if not click [here](https://docs.docker.com/engine/reference/commandline/login/))
