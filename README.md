# confd-data-container-generator

[![build status](https://gitlab.com/nodevops/confd-data-container-generator/badges/master/build.svg)](https://gitlab.com/nodevops/confd-data-container-generator/commits/master)

```bash

    usage: generate-data-image.sh parameters options

    Generate a docker image with the configuration in a specific volume.
    Publish the resulting image to Docker Store

    PARAMETERS:
       -g --git-repository           url of the git repository holding the configuration
       -v --env-version              configuration version to retrieve
       -e --env-name                 name of the env-name to generate the configuration for

    OPTIONS:
       -h --help                     print this help
       -w --working-directory        specify the working directory inside the image (optional)
                                      (default value: /var/tmp)
       -f --config-file-name         name of the output file generated
                                      (default value: env.sh)
       -d --config-base-directory    folder where the generated file will be generated
                                      (default value: /config)
       -i --image-name               docker image name (pushed with a tag associated to the env and the version values)
                                      (default value: env-data:$env--$version)
       -p --publish-image            publish the image to the associated docker registry
       -n --no-cache                 bypass docker build cache
       -q --quiet                    Suppress output and print image ID on success

    Examples:
       generate-data-image.sh -v v0.1.0 -e master --git-repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git

```
