#!/usr/bin/env bash

source ../setup/libs/utils.sh

TEST_HOME=$(pwd)
TEST_DIRECTORY=temp/

test_directory_existed() {
    local dir="tmp"
    assertFalse "$FUNCNAME: Test folder not created"    "is_here ${dir} ${!dir@}"
    mkdir "${dir}"
    assertTrue "$FUNCNAME: Test folder created"         "is_here ${dir} ${!dir@}"
    rm -r "${dir}"
    assertFalse "$FUNCNAME: Test folder removed"        "is_here ${dir} ${!dir@}"
}

test_directory_writeable() {
    local dir="tmp"
    mkdir "${dir}"
    assertTrue "$FUNCNAME: Test folder created"             "is_writeable ${dir} ${!dir@}"
    chmod a-w "${dir}"
    assertFalse "$FUNCNAME: Test folder unwritable"         "is_writeable ${dir} ${!dir@}"
    chmod a+w "${dir}"
    assertTrue "$FUNCNAME: Test folder writeable again"     "is_writeable ${dir} ${!dir@}"
}

test_global_architecture() {
    OUT_DIR=$(pwd)/conf/
    OUT_DIR_DATA=${OUT_DIR}/data/
    OUT_DIR_DICT=${OUT_DIR}/dict/
    WORK_DIR=$(pwd)/workspace/
    assertFalse "$FUNCNAME: Test folders not created"   "check_architecture"
    mkdir -p "${OUT_DIR}" "${OUT_DIR_DATA}" "${OUT_DIR_DICT}" "${WORK_DIR}"
    assertTrue "$FUNCNAME: Test folders created"        "check_architecture"
}

test_import_repo() {
    WORK_DIR=$(pwd)/workspace/
    TAG=v0.1.0_dev-local
    REPO=https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git
    assertFalse "$FUNCNAME: Working folder not created"     "import_repo ${TAG} ${REPO}"
    mkdir -p "${WORK_DIR}"
    assertTrue "$FUNCNAME: Working folder created"          "import_repo ${TAG} ${REPO}"
}

test_generate_file_env() {
    ENV=dev-local
    TAG=v0.1.0_${ENV}
    REPO="https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    OUT_DIR_DICT=$(pwd)/conf/dict/
    OUTPUT_FILE_CONFIG_NAME="env.sh"
    WORK_DIR=$(pwd)/workspace/
    assertFalse "$FUNCNAME: Test folder not created"        "generate_file_env ${OUTPUT_FILE_CONFIG_NAME}"
    mkdir -p "${WORK_DIR}"
    import_repo > /dev/null 2> /dev/null
    mkdir -p "${OUT_DIR_DICT}"
    cd ..
    assertTrue "$FUNCNAME: Test folder created"             "generate_file_env ${OUTPUT_FILE_CONFIG_NAME}"
}

test_merge_data() {
    WORK_DIR=$(pwd)/workspace/
    OUT_DIR_DATA=$(pwd)/conf/data/
    ENV=dev-local
    TAG=v0.1.0_${ENV}
    REPO=https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git
    assertFalse "$FUNCNAME: Test folder not created"        "merge_data"
    mkdir "${WORK_DIR}"
    import_repo > /dev/null 2> /dev/null
    mkdir -p "${OUT_DIR_DATA}"
    assertTrue "$FUNCNAME: Test folder created"             "merge_data"
}

test_check_initialized() {
    VARIABLE=
    assertFalse "$FUNCNAME: Not initialized"     "check_initialized ${!VARIABLE@} ${VARIABLE}"
    VARIABLE="test"
    assertTrue "$FUNCNAME: Initialized"          "check_initialized ${!VARIABLE@} ${VARIABLE}"
}

test_args_manager() {
    # shellcheck disable=SC2034
    REPO=
    # shellcheck disable=SC2034
    ENV=
    # shellcheck disable=SC2034
    APP_VERSION=
    #TODO: update when change one flag
    # Valid flags
    assertTrue "$FUNCNAME: Short flag: help"                        "args_manager -h"
    assertTrue "$FUNCNAME: Long flag: help"                         "args_manager --help"
    assertTrue "$FUNCNAME: example command short options"           "args_manager -e dev-local -v v0.1.0 -r https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: example command long options"            "args_manager --environment dev-local --app-version v0.1.0 --repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"

    assertTrue "$FUNCNAME: 3 -> Short flag: working-directory"      "args_manager -w ./tmp                       -e dev-local -v v0.1.0 --repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: 3 -> Long flag: working-directory"       "args_manager --working-directory ./tmp      -e dev-local -v v0.1.0 --repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: 3 -> Short flag: config-file-name"       "args_manager -n env.sh                      -e dev-local -v v0.1.0 --repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: 3 -> Long flag: config-file-name"        "args_manager --config-file-name env.sh      -e dev-local -v v0.1.0 --repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: 3 -> Short flag: out-directory"          "args_manager -o ./config                    -e dev-local -v v0.1.0 --repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertTrue "$FUNCNAME: 3 -> Long flag: out-directory"           "args_manager --out-directory ./config       -e dev-local -v v0.1.0 --repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    # Invalid 1 flags
    assertFalse "$FUNCNAME: 1 -> Short flag: repo"                  "args_manager -r https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertFalse "$FUNCNAME: 1 -> Long flag: repo"                   "args_manager --repository https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git"
    assertFalse "$FUNCNAME: 1 -> Short flag: app-version"           "args_manager -v v0.1.0"
    assertFalse "$FUNCNAME: 1 -> Long flag: app-version"            "args_manager --app-version v0.1.0"
    assertFalse "$FUNCNAME: 1 -> Short flag: environment"           "args_manager -e dev-local"
    assertFalse "$FUNCNAME: 1 -> Long flag: environment"            "args_manager --environment dev-local"

    assertFalse "$FUNCNAME: 1 -> Short flag: working-directory"     "args_manager -w ./tmp"
    assertFalse "$FUNCNAME: 1 -> Long flag: working-directory"      "args_manager --working-directory ./tmp"
    assertFalse "$FUNCNAME: 1 -> Short flag: config-file-name"      "args_manager -n env.sh"
    assertFalse "$FUNCNAME: 1 -> Long flag: config-file-name"       "args_manager --config-file-name env.sh"
    assertFalse "$FUNCNAME: 1 -> Short flag: out-directory"         "args_manager -o ./config"
    assertFalse "$FUNCNAME: 1 -> Long flag: out-directory"          "args_manager --out-directory ./config"
    # Invalid flags
    assertFalse "$FUNCNAME: Short flag: unknown"                    "args_manager -k"
    assertFalse "$FUNCNAME: Long flag: unknown"                     "args_manager --unknown-flag"
}

oneTimeSetUp() {
    cd "${TEST_HOME}"
    cp ././../setup/generate-file-env.py .
    rm -rf "${TEST_DIRECTORY}"
    mkdir "${TEST_DIRECTORY}"
}

setUp () {
    cd "${TEST_HOME}"/"${TEST_DIRECTORY}"
}

oneTimeTearDown() {
    cd "${TEST_HOME}"
    rm -rf "${TEST_DIRECTORY}"
    rm generate-file-env.py
}

tearDown() {
    cd "${TEST_HOME}"/"${TEST_DIRECTORY}"
    rm -rf ./*
}

# load shunit2
. shunit2
