FROM alpine:3.5

RUN apk add --no-cache \
    bash \
    python \
    git

COPY setup/ /usr/local/bin/

ARG WORKDIR=/var/tmp/
ARG OUTDIR=/config
ARG REPOSITORY=https://gitlab.com/cfurmaniak/guestbook-configuration-confd.git
ARG VERSION=v0.1.1
ARG ENVIRONMENT=dev-local
ARG CONFIG_FILE_NAME=env.sh
ARG BUILD_DATE=FIXME

LABEL maintainers="Jules.Hablot@zenika.com,christophe.furmaniak@zenika.com" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.name="data-image-${ENVIRONMENT}--${VERSION}" \
      org.label-schema.description="data image for env '${ENVIRONMENT}' at version '${VERSION}'" \
      org.label-schema.vcs-url="$REPOSITORY" \
      org.label-schema.vcs-ref="${VERSION}_${ENVIRONMENT}" \
      org.label-schema.schema-version="1.0"

ENV OUTDIR=$OUTDIR \
    REPOSITORY=$REPOSITORY \
    VERSION=$VERSION \
    ENVIRONMENT=$ENVIRONMENT \
    CONFIG_FILE_NAME=$CONFIG_FILE_NAME

RUN mkdir -p $OUTDIR/data $OUTDIR/dictionaries

RUN create-conf.sh \
        --out-directory=$OUTDIR \
        --working-directory=$WORKDIR \
        --repository=$REPOSITORY \
        --app-version=$VERSION \
        --environment=$ENVIRONMENT \
        --config-file-name=$CONFIG_FILE_NAME

VOLUME $OUTDIR
